##  Process Pension Service
* It takes in the aadhaar number of the pensioner
* Verifies if the pensioner detail is accurate by getting the data from PensionerDetail Microservice or not. 
* If valid, then pension calculation is done and the pension detail is returned to the Web application to be displayed on the UI.

### This service provides one controller end-points:
 1.It calls the Pension Details webservice and takes data from that webservice and caluculates the PensionAmount and Bankcharge

**Valid Input**

```
{
  "aadhaarNumber": "123456789012",
 
}
```
**Response for valid input**

```
{
  "pensionAmount": 31600
   "bankCharge: "500"
}
```

**Invalid Input**
```
{
  "aadhaarNumber": "123456789012",
}
```

**Response for invalid input**
```
{
    "title":"Not Found"
    "Status Code:"404"
}
```
